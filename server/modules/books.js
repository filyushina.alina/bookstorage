const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const {
  ObjectId
} = mongoose.Schema.Types;

const books = Schema({
  _id: ObjectId,
  name: { type: String, index: true },
  authors: [{ type: ObjectId, ref: 'Author', index: true }],
  pages: Number
});

books.index({ '$**': 'text' });

module.exports = mongoose.model('Book', books);
