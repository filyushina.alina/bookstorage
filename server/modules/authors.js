const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const {
  ObjectId
} = mongoose.Schema.Types;

const authors = Schema({
  _id: Schema.Types.ObjectId,
  name: { type: String, index: true },
  books: [{ type: ObjectId, ref: 'Book' }]
});

authors.index({ '$**': 'text' });

module.exports = mongoose.model('Author', authors);
