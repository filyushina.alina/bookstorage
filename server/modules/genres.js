const mongoose = require('mongoose');

const genres = new mongoose.Schema({
  id: String,
  name: String
});

module.exports.Genre = mongoose.model('Genre', genres);
