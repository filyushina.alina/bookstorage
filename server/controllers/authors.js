const router = require('koa-joi-router');
const Joi = router.Joi;
const AuthorModel = require('../modules/authors.js');

const authorsRouter = router();
const mongoose = require('mongoose');

const {
  ObjectId
} = mongoose.Types;

function getAuthors () {
  return AuthorModel.find({}).select('_id');
}

async function getAuthorInfo (authorId) {
  return AuthorModel
    .findById(authorId.authorId)
    .populate('books')
    .exec();
}

async function createAuthor (author) {
  const authorObject = {
    name: author.name
  };

  let auth = new AuthorModel(authorObject);
  auth.save();
  return author;
}

async function deleteAuthor (author) {
  const authorObject = {
    _id: author.authorId
  };

  return AuthorModel.deleteOne(authorObject, function (err) {
    if (err) throw err;
  }).exec();
}

async function updateAuthor (author, authorId) {
  let id = new ObjectId(authorId.authorId);

  const query = {
    _id: id
  };

  return AuthorModel.update(query, { $set: author }).exec();
}

authorsRouter.route({
  method: 'get',
  path: '/authors',
  handler: async (ctx) => {
    const result = getAuthors();
    ctx.status = 200;
    ctx.body = result;
  }
});

authorsRouter.route({
  method: 'get',
  path: '/authors/:authorId',
  validate: {
    params: {
      authorId: Joi.string()
    }
  },
  handler: async (ctx) => {
    const result = await getAuthorInfo(ctx.request.params);
    ctx.status = 200;
    ctx.body = result;
  }
});

authorsRouter.route({
  method: 'post',
  path: '/authors',
  validate: {
    body: {
      _id: Joi.string().guid(),
      name: Joi.string().max(100).required()
    },
    type: 'json'
  },
  handler: async (ctx) => {
    const result = await createAuthor(ctx.request.body);
    ctx.status = 201;
    ctx.body = result;
  }
});

authorsRouter.route({
  method: 'delete',
  path: '/authors/:authorId',
  validate: {
    params: {
      authorId: Joi.string()
    }
  },
  handler: async (ctx) => {
    const result = await deleteAuthor(ctx.request.params);
    ctx.status = 200;
    ctx.body = result;
  }
});

authorsRouter.route({
  method: 'put',
  path: '/authors/:authorId',
  validate: {
    params: {
      authorId: Joi.string()
    },
    body: {
      name: Joi.string().max(100)
    },
    type: 'json',
    output: {
      200: {
        body: {
          bookId: Joi.string(),
          name: Joi.string()
        }
      }
    }
  },
  handler: async (ctx) => {
    let result = await updateAuthor(ctx.request.body, ctx.request.params);
    ctx.status = 201;
    ctx.body = result;
  }
});

module.exports = authorsRouter.middleware();
