const router = require('koa-joi-router');
const Joi = router.Joi;
const BookModel = require('../modules/books.js');
const mongoose = require('mongoose');

const {
  ObjectId
} = mongoose.Types;

const booksRouter = router();

async function createBook (book) {
  const bookObject = {
    name: book.name,
    authors: book.authors,
    pages: book.pages
  };

  if (book.id) {
    bookObject._id = book.id;
  }

  let theBook = new BookModel(bookObject);
  theBook.save();
  return book;
}

async function getBooks (query) {
  const {
    pageSize,
    page,
    search
  } = query;

  const searchObject = {
    $text: {
      $search: search
    }
  };

  const skipNum = pageSize * (page - 1);

  return BookModel
    .find(searchObject)
    .skip(skipNum)
    .limit(pageSize);
}

async function getBookInfo (bookId) {
  return BookModel
    .findById(bookId.bookId)
    .populate('authors')
    .exec();
}

async function updateBook (book, bookId) {
  console.log(book);
  let id = new ObjectId(bookId.bookId);

  const query = {
    _id: id
  };

  return BookModel
    .update(query, { $set: book }
      .exec());
}

async function deleteBook (book) {
  const bookObject = {
    _id: book.bookId
  };

  return BookModel
    .deleteOne(bookObject, function (err) {
      if (err) throw err;
    })
    .exec();
}

booksRouter.route({
  method: 'post',
  path: '/books',
  validate: {
    body: {
      id: Joi.string().guid(),
      name: Joi.string().max(100).required(),
      authors: Joi.array().items(Joi.string()).required(),
      pages: Joi.number().integer().required()
    },
    type: 'json',
    output: {
      200: {
        body: {
          bookId: Joi.string(),
          name: Joi.string()
        }
      }
    }
  },
  handler: async (ctx) => {
    let result = await createBook(ctx.request.body);
    ctx.status = 201;
    ctx.body = result;
  }
});

booksRouter.route({
  method: 'get',
  path: '/books',
  validate: {
    query: {
      page: Joi.number(),
      pageSize: Joi.number(),
      search: Joi.string()
    }
  },
  handler: async (ctx) => {
    let result = await getBooks(ctx.request.query);
    ctx.status = 200;
    ctx.body = result;
  }
});

booksRouter.route({
  method: 'get',
  path: '/books/:bookId',
  validate: {
    params: {
      bookId: Joi.string()
    }
  },
  handler: async (ctx) => {
    let result = await getBookInfo(ctx.request.params);
    ctx.status = 200;
    ctx.body = result;
  }
});

booksRouter.route({
  method: 'put',
  path: '/books/:bookId',
  validate: {
    params: {
      bookId: Joi.string()
    },
    body: {
      _id: Joi.string(),
      name: Joi.string().max(100),
      authors: Joi.array().items(Joi.string()),
      pages: Joi.number()
    },
    type: 'json',
    output: {
      200: {
        body: {
          bookId: Joi.string(),
          name: Joi.string()
        }
      }
    }
  },
  handler: async (ctx) => {
    let result = await updateBook(ctx.request.body, ctx.request.params);
    ctx.status = 201;
    ctx.body = result;
  }
});

booksRouter.route({
  method: 'delete',
  path: '/books/:bookId',
  validate: {
    params: {
      bookId: Joi.string()
    }
  },
  handler: async (ctx) => {
    let result = await deleteBook(ctx.request.params);
    ctx.status = 200;
    ctx.body = result;
  }
});

module.exports = booksRouter.middleware();
