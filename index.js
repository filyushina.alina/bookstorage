const Koa = require('koa');
const mongoose = require('mongoose');
const books = require('./server/controllers/books.js');
const authors = require('./server/controllers/authors.js');

const app = new Koa();

mongoose.connect('mongodb://localhost:27017/bookStorage', { useNewUrlParser: true }, function (err) {
  if (err) throw err;
  console.log('MongoDB successfully connected');
});

app.use(books);
app.use(authors);



app.listen(3002);
